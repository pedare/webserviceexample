CREATE DATABASE wsedb CHARACTER SET UTF8;
CREATE USER wseuser@localhost IDENTIFIED BY 'wseuser-password';
GRANT ALL PRIVILEGES ON vsedb.* TO vseuser@localhost;
FLUSH PRIVILEGES;
exit
