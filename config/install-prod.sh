#!/usr/bin/bash

echo
echo "### Check we are running with sudo privilages (ie: as root)"
echo
if [ $EUID -ne 0 ]; then
  echo "ERROR: installer needs to be run with sudo/root privilages (try 'sudo')"
  echo
  exit -1
fi

echo
echo "### Ensure Centos7 is up to date"
echo 
yum upgrade -y
yum update -y

echo
echo "### Install the required packages"
echo

echo
echo "### Get the EPEL release (to allow access to python36)"
echo
yum install -y epel-release

echo
echo "### Get the web server package (which isn’t available by default in Centos7)"
echo
rpm -ivh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm

echo
echo "### Install yum packages"
echo
yum install -y python36 python36-devel gcc mariadb-server mariadb-devel nginx uwsgi git emacs


echo
echo "### Install pip packages"
echo
python36 -m ensurepip --default-pip
python36 -m pip install virtualenv virtualenvwrapper uwsgi mysqlclient
python36 -m pip install --upgrade pip

echo
echo "### Create the production user (wse)"
echo 
useradd -d /home/wse wse
usermod -a -G wse nginx
chmod 710 /home/wse

echo
echo "### Download the wse software from bitbucket"
echo
mkdir /home/wse/deploy
cd /home/wse/deploy
git clone https://bitbucket.org/pedare/webserviceexample.git
cd /home/wse/deploy/webserviceexample

echo
echo "### Set the wse production environment configuration (before starting the database)"
echo
export WSE_PROD_DEPLOY=true

echo
echo "### Start the mariadb database"
echo
systemctl start mariadb
systemctl enable mariadb

echo
echo "### Create the 'database system admin' (please enter a strong password and write it down in a safe place!)"
echo
mysql_secure_installation

echo
echo "### Create the wse database instance (you'll need to enter the 'database system admin' password from above)"
echo
mysql -u root -p < /home/wse/deploy/webserviceexample/config/createdb.sql

echo
echo "### Remove any existing virtual environment (that is often checked in accidentally)"
echo
rm -rf venv

echo
echo "### Create the Linux virtual environment"
echo
python36 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
pip install --upgrade pip

echo
echo "### Create the wse database"
echo
cd /home/wse/deploy/webserviceexample/app
rm app/migrations/0*.py
python manage.py makemigrations
python manage.py migrate

echo
echo "### Create the 'wse admin' user (please enter a strong password and write it down in a safe place!)"
echo
python manage.py createsuperuser --username=admin --email=admin@example.com

echo
echo "### wse software configured"
echo
deactivate

echo
echo "### Updating wse user file permissions"
echo
chown -R wse /home/wse
chgrp -R nginx /home/wse

echo 
echo "### Starting nginx web server"
echo
systemctl start nginx
systemctl enable nginx

echo
echo "### Configuring firewall to allow HTTP and HTTPS"
echo
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload

echo
echo "### Configuring uwsgi (web server to python integration)"
echo
mkdir -p /etc/uwsgi/sites
cp /home/wse/deploy/webserviceexample/config/uwsgi.ini /etc/uwsgi/sites
cp /home/wse/deploy/webserviceexample/config/uwsgi.service /etc/systemd/system/
chmod +x /etc/systemd/system/uwsgi.service
mkdir -p /run/uwsgi
chown wse:nginx /run/uwsgi

echo
echo "### Configuring nginx"
echo
mkdir -p /etc/pki/nginx 
cp /home/wse/deploy/webserviceexample/config/wse.crt /etc/pki/nginx 
cp /home/wse/deploy/webserviceexample/config/wse.key /etc/pki/nginx 
cp /home/wse/deploy/webserviceexample/config/nginx.conf /etc/nginx/nginx.conf 
chcon -Rt httpd_sys_content_t /home/wse/deploy/webserviceexample/app/app/static
systemctl restart nginx

echo
echo "### Starting uwsgi"
echo
systemctl daemon-reload
systemctl start uwsgi
systemctl enable uwsgi

echo
echo "### wse system installation complete"
echo

